package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"time"
)

type Order struct {
	gorm.Model
	Description string
	Status string `gorm:"not null"`
	EmployeeID int
}

type Employee struct {
	gorm.Model
	Username string `gorm:"not null"`
	Fullname string `gorm:"not null"`
	Email string `gorm:"not null"`
	Orders []Order `gorm:"ForeignKey:EmployeeID"`
}

func main() {

	log, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	defer log.Sync() // flushes buffer, if any

	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", "postgres", "postgres", "localhost:32768", "sample")

	db, err := gorm.Open("postgres", connStr)
	if err != nil {
		log.Fatal(errors.Wrap(err, "failed to connect database").Error())
	}
	defer db.Close()

	// Flushing the the old data (if it's needed) and migrate the schema
	if err := db.Set("gorm:table_options", "CASCADE").DropTableIfExists(&Order{}, &Employee{}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot drop tables").Error())
	}
	if err = db.AutoMigrate(&Employee{}, &Order{}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot automigrate schema").Error())
	}
	if err = db.Model(&Order{}).AddForeignKey("employee_id", "employees(id)", "CASCADE", "CASCADE").Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot add foreign key").Error())
	}

	// Create
	if err = db.Create(&Employee{
		Model: gorm.Model{
			ID: 1,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		Username: "someUsrNm1",
		Fullname: "someFNm1",
		Email: "some1@email.ru",
		Orders: []Order{
			{
				Model: gorm.Model{
					ID: 1,
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				},
				Description: "some order 1",
				Status: "completed",
			},
			{
				Model: gorm.Model{
					ID: 2,
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				},
				Description: "some order 2",
				Status: "work in progress",
			},
		},
	}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot insert record").Error())
	}
	if err = db.Create(&Employee{
		Model: gorm.Model{
			ID: 2,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		Username: "someUsrNm2",
		Fullname: "someFNm2",
		Email: "some2@email.ru",
	}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot insert record").Error())
	}
	if err = db.Create(&Employee{
		Model: gorm.Model{
			ID: 3,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		Username: "someUsrNm3",
		Fullname: "someFNm3",
		Email: "some3@email.ru",
	}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot insert record").Error())
	}
	if err = db.Create(&Employee{
		Model: gorm.Model{
			ID: 4,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		Username: "someUsrNm4",
		Fullname: "someFNm4",
		Email: "some4@email.ru",
	}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot insert record").Error())
	}

	// Read
	var employee1, employee2 Employee
	if err = db.First(&employee1, "ID = ?", 1).Find(&employee1.Orders, "employee_id = ?", 1).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot get record").Error())
	}
	if err = db.First(&employee2, "ID = ?", 2).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot get record").Error())
	}

	// Update - update employee's fullname
	if err = db.Model(&employee2).Update("Fullname", "someLongerFName2").Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot update record").Error())
	}
	// Update - update employee's orders -> soft deleting
	if err = db.First(&employee1, "ID = ?", 1).Find(&employee1.Orders, "employee_id = ?", 1).Delete(Order{}, "ID = ?", employee1.Orders[0].ID).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot update record").Error())
	}

	if err = db.Find(&employee1, "ID = ?", 1).Find(&employee1.Orders, "employee_id = ?", 1).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot get record").Error())
	}
	log.Info(employee1.Fullname)
	for i := range employee1.Orders {
		log.Info(employee1.Orders[i].Description)
	}

	// this is why I don't like sqlx
	log.Info("selected users")
	emps1 := []Employee{}
	if err = db.Find(&emps1, "ID in (?,?,?)", 1, 3, 4).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot get records").Error())
	}
	for i := range emps1 {
		log.Info(emps1[i].Username)
	}

	log.Info("selected users")
	emps2 := []Employee{}
	if err = db.Find(&emps2, "ID in (?)", []int{1,3,4}).Error; err != nil {
		log.Fatal(errors.Wrap(err, "cannot get records").Error())
	}
	for i := range emps2 {
		log.Info(emps2[i].Username)
	}
}
